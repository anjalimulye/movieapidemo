const API_KEY = '<<API_KEY>>';
const API_URL = 'https://api.themoviedb.org/3/movie/upcoming?api_key='+API_KEY;
const image_url = 'https://image.tmdb.org/t/p/w500';
const search_url = 'https://api.themoviedb.org/3/search/movie?api_key='+API_KEY;

const main = document.getElementById('main');
const form =  document.getElementById('form');
const search = document.getElementById('search');

getUpcomingMovies(API_URL);

function getUpcomingMovies(url) 
{
    fetch(url).then(res => res.json()).then(data => 
	{
        console.log(data.results)
		displayMovieImg(data.results);            
    })
}

function displayMovieImg(data) 
{
    main.innerHTML = '';

    data.forEach(movie => 
	{
        const {poster_path, title, vote_average, overview} = movie;
        const upcmg_movie = document.createElement('div');
        upcmg_movie.classList.add('movie');
        upcmg_movie.innerHTML = `
             <img src="${poster_path? image_url + poster_path : "http://via.placeholder.com/1080x1580" }" >
			 
			 <div class="movie_title">
                <h3>${title}</h3>
                <span class="vote_average" color:"#FF0000">${vote_average}</span>						
            </div>					
			
        `
        main.appendChild(upcmg_movie);        
    })
}

form.addEventListener('submit', (e) => 
{
    e.preventDefault();

    const search_value = search.value;

    if(search_value) 
	{
        getUpcomingMovies(search_url + '&query=' + search_value)
    }
	else
	{
        getUpcomingMovies(API_URL);
    }

})

